import java.util.*;

import org.instructures.ArgsParser;
import org.instructures.Operand;
import org.instructures.Option;

import java.io.*;

public class HuffmanCodes {
	private static final String SUMMARY = "Encodes and decodes files using Huffman's technique";

	private static Map<Byte, Integer> counts = new HashMap<Byte, Integer>();
	private static PriorityQueue<Node> tree;
	private static File in;
	private static File out;

	// counts size of header for output
	private static int headerCount;

	private static Node parentNode;
	private static Map<Byte, String> key;

	// argsparser variables
	private static Option ENCODE, DECODE;
	private static Option SHOW_FREQUENCY, SHOW_CODES, SHOW_BINARY;
	private static ArgsParser parser;
	private static Operand<File> IN, OUT;

	private static ArgsParser.Bindings settings;

	public static void main(String args[]) throws FileNotFoundException,
			IOException {

		ENCODE = Option.create("-e, --encode").summary("Encodes IN to OUT");
		DECODE = Option.create("-d, --decode").summary("Decodes IN to OUT");
		SHOW_FREQUENCY = Option.create("--show-frequency")
				.associatedWith(ENCODE).summary("Output byte frequencies");
		SHOW_CODES = Option.create("--show-codes").summary(
				"Output the code for each byte");
		SHOW_BINARY = Option
				.create("--show-binary")
				.associatedWith(ENCODE)
				.summary(
						"Output a base-two representation of the encoded sequence");

		parser = ArgsParser.create("HuffmanCodes")
				.summary("Encodes and decodes files using Huffman's technique")
				.helpFlags("-h, --help");
		IN = Operand.create(File.class, "IN");
		OUT = Operand.create(File.class, "OUT");
		parser.requireOneOf("mode required", ENCODE, DECODE)
				.optional(SHOW_FREQUENCY).optional(SHOW_CODES)
				.optional(SHOW_BINARY).requiredOperand(IN).requiredOperand(OUT);

		settings = parser.parse(args);

		try (BitInputStream inStream = new BitInputStream(
				settings.getOperand(IN));
				BitOutputStream outStream = new BitOutputStream(
						settings.getOperand(OUT))) {
			in = settings.getOperand(IN);

			if (settings.hasOption(ENCODE)) {

				setCount(inStream.allBytes());
				buildForest();
				if (settings.hasOption(SHOW_FREQUENCY)) {
					printFrequency();
				}
				Node parent = createTree();
				key = parent.getAllCodes();

				if (settings.hasOption(SHOW_CODES)) {
					printCodes(key);
				}

				if (settings.hasOption(SHOW_BINARY)) {
					encode(inStream, outStream, true);

				} else {
					encode(inStream, outStream, false);
				}
				outStream.close();

				// decode(inStream, settings.getOperand(OUT));

			} else {
				
				
				if(settings.hasOption(SHOW_BINARY)){
					decode(inStream, settings.getOperand(OUT), true);
				}
				else{
					decode(inStream, settings.getOperand(OUT),false);
				}
				if (settings.hasOption(SHOW_CODES)) {
					printCodes(key);
				}
			}
		} catch (Exception e) {
			System.err.printf("Error: %s%n", e.getMessage());
			System.exit(1);
		}

	}

	private static void setCount(byte[] in) {

		for (int i = 0; i < in.length; i++) {

			if ((int) in[i] == 10) {
				System.out.println("newLine found");
			}

			if (!counts.containsKey(in[i])) {
				counts.put(in[i], 1);
			} else {
				counts.put(in[i], counts.get(in[i]) + 1);
			}
		}
	}

	private static void buildForest() {
		PriorityQueue<Node> forest;
		if (counts.size() > 1) {
			forest = new PriorityQueue<Node>(counts.size());
		} else {
			forest = new PriorityQueue<Node>(1);
		}

		for (Map.Entry<Byte, Integer> temp : counts.entrySet()) {
			Node n = new Node(temp.getKey(), temp.getValue());
			forest.add(n);
		}

		tree = forest;
	}

	private static Node createTree() {

		Node left;
		Node right;

		// if has more than 1 value
		while (tree.size() > 1) {
			left = tree.remove();
			right = tree.remove();
			parentNode = new Node(left.value + right.value);
			parentNode.leftNode = left;
			parentNode.rightNode = right;
			tree.add(parentNode);

		}

		return tree.remove();
	}

	private static void encode(BitInputStream input, BitOutputStream output,
			boolean showSequence) throws IOException {

		// calculate size of key
		int size = 0;
		Collection<Integer> c = counts.values();
		for (Integer temp : c) {
			size += temp;
		}
		output.writeInt(size);

		// 32 bit size of input file
		headerCount = 32;

		parentNode.writeTo(output);

		byte[] allBytes = input.allBytes();

		String sequence = "";

		int encodingCount = 0;

		for (int i = 0; i < allBytes.length; i++) {

			String keyVal = key.get(allBytes[i]);
			sequence += keyVal;

			for (int j = 0; j < keyVal.length(); j++) {
				output.writeBit(Integer.parseInt("" + keyVal.charAt(j)));
				encodingCount++;
			}

		}
	
		if (showSequence) {
			System.out.println("ENCODED SEQUENCE");
			System.out.print(sequence);
		}

		// print number of bits in input'
		// remove stupid character
		System.out.printf("\ninput: %d bytes [%d bits]",
				input.allBytes().length, (input.allBytes().length) * 8);

		// print number of bits in output
		System.out.printf(
				"\noutput: %d bytes [header: %d bits; encoding: %d bits]\n",
				(int) Math.ceil((double) output.tally() / 8), headerCount,
				encodingCount);

	}

	private static void decode(BitInputStream encodedFile, File out, boolean showSequence)
			throws IOException {
		Node parent = new Node(1);
		PrintWriter output = new PrintWriter(out);
		// skip size
		int originalSize = encodedFile.readInt();

		parent.constructDecodeTree(encodedFile);

		key = parent.getAllCodes();

		printCodes(key);

		// next bits are code
		// loops through original size of array
		
		String sequence = "";
		for (int i = 0; i < originalSize; i++) {
			String nextBits = "" + encodedFile.readBit();
			sequence += nextBits;
			while (!key.containsValue(nextBits)) {
				nextBits += encodedFile.readBit();

			}
			// System.out.println(nextBits);

			for (Map.Entry<Byte, String> temp : key.entrySet()) {
				if (temp.getValue().equals(nextBits)) {
					output.print((char) temp.getKey().intValue());
					break;
				}
			}
		}
		
		if(showSequence){
			System.out.println("ENCODED SEQUENCE");
			System.out.println(sequence);
		}

		output.close();

	}

	private static void printFrequency() {
		PriorityQueue<Node> temp = new PriorityQueue<Node>(tree);

		System.out.println("FREQUENCY TABLE");

		while (!temp.isEmpty()) {
			Node removed = temp.remove();

			// convert to char
			byte b = removed.key;
			char read = (char) b;
			int count = removed.value;

			System.out.printf("'%s' : %d%n", read, count);
		}
	}

	private static void printCodes(Map<Byte, String> getAll) {
		System.out.println("CODES");
		// sort array
		Byte[] temp = new Byte[getAll.size()];
		getAll.keySet().toArray(temp);

		for (Byte keyVal : temp) {
			String c = getAll.get(keyVal);
			System.out.printf("\"%s\" -> %s\n", c, (char) (keyVal.intValue()));
		}
	}

	private static class Node implements Comparable<Node> {
		private Node leftNode;
		private Node rightNode;
		Byte key;
		Integer value;

		// non-leaf node
		public Node(int count) {
			value = count;
			key = null;
		}

		// if value is a leaf
		public Node(byte b, int count) {
			key = b;
			value = count;

		}

		public void writeTo(BitOutputStream out) throws IOException {
			// leaf
			if (this.leftNode == null && this.rightNode == null) {
				headerCount += 9;
				out.writeBit(1);
				out.writeByte(key);
			} else {
				out.writeBit(0);
				headerCount += 1;
			}

			if (leftNode != null) {

				leftNode.writeTo(out);
			}
			if (rightNode != null) {

				rightNode.writeTo(out);
			}

		}

		public Map<Byte, String> getAllCodes() {
			Map<Byte, String> codeTable = new LinkedHashMap<>();
			this.putCodes(codeTable, "");
			return codeTable;
		}

		public void putCodes(Map<Byte, String> table, String bits) {

			// reached end
			if (leftNode == null && rightNode == null) {

				table.put(this.key, bits);

			}
			if (leftNode != null) {

				leftNode.putCodes(table, bits + "0");
			}
			if (rightNode != null) {

				rightNode.putCodes(table, bits + "1");

			}
		}

		private void constructDecodeTree(BitInputStream encodedFile)
				throws IOException {

			// node is a leaf
			// first zero
			int nextBit = encodedFile.readBit();

			if (nextBit == 0) {
				leftNode = new Node(1);
				rightNode = new Node(1);
				leftNode.constructDecodeTree(encodedFile);
				rightNode.constructDecodeTree(encodedFile);

			}

			else {
				int byteVal = encodedFile.readByte();
				this.key = (byte) byteVal;

			}
		}

		@Override
		public int compareTo(Node o) {
			return this.value - o.value;
		}

		public String toString() {
			return "key: " + key;
		}

	}
}